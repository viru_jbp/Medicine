package com.online.medicine.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.online.medicine.R;

import com.online.medicine.application.MedicineApp;
import com.online.medicine.fragments.DescriptionFragment;
import com.online.medicine.fragments.MyCartFragment;
import com.online.medicine.fragments.MyOrdersFragment;
import com.online.medicine.listeners.FragmentInteractionListener;
import com.online.medicine.utils.AppPreference;
import com.online.medicine.utils.Constants;

import java.util.Date;

public class MainActivity extends AppCompatActivity
        implements FragmentInteractionListener, NavigationView.OnNavigationItemSelectedListener {

    private TabLayout.Tab tab;
    private TabLayout tabLayout;
    private boolean tabReselect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if(!MedicineApp.skipLogin && !AppPreference.getBoolean(Constants.IS_LOGIN, this)){
            final Intent intent = new Intent(this.getApplicationContext(), LoginActivity.class);
            this.startActivity(intent);
            this.finish();
            return;
        }
        startNewFragment(new DescriptionFragment(), Constants.DESCRIPTION_FRAGMENT);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
       // drawer.openDrawer(GravityCompat.START);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        setTabLayout(tabLayout);
        tab = tabLayout.getTabAt(0);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_my_account) {
            onFragmentInteraction(Constants.MY_ACCOUNT_FRAGMENT, null);
        } else if (id == R.id.nav_my_cart) {
            onFragmentInteraction(Constants.MY_CART_FRAGMENT, null);
        } else if (id == R.id.nav_my_orders) {
            onFragmentInteraction(Constants.MY_ORDERS_FRAGMENT, null);
        } else if (id == R.id.nav_logout) {
            onFragmentInteraction(Constants.LOGOUT_FRAGMENT, null);
        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onFragmentInteraction(String id, Object data) {
        switch (id) {
            case Constants.MY_CART_FRAGMENT:
                popBackStack();
                startNewFragment(new MyCartFragment(), Constants.MY_CART_FRAGMENT);
                break;
            case Constants.MY_ORDERS_FRAGMENT:
                popBackStack();
                startNewFragment(new MyOrdersFragment(), Constants.MY_ORDERS_FRAGMENT);
                break;
            case Constants.MY_ACCOUNT_FRAGMENT:
                popBackStack();
                startNewFragment(new DescriptionFragment(), Constants.DESCRIPTION_FRAGMENT);
                break;
            case Constants.LOGOUT_FRAGMENT:
                popBackStack();
                startNewFragment(new DescriptionFragment(), Constants.DESCRIPTION_FRAGMENT);
                break;
        }
    }

    private void startNewFragment(final Fragment frag, final String tag) {
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (this.getSupportFragmentManager().findFragmentById(R.id.container) != null) {
            fragmentTransaction.replace(R.id.container, frag, tag);
            fragmentTransaction.addToBackStack(null);
        } else {
            fragmentTransaction.add(R.id.container, frag, tag);
        }
        //fragmentTransaction.commit();
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void setTabLayout(TabLayout tabLayout) {
        tabLayout.addTab(tabLayout.newTab().setCustomView(getCustomTabView(R.drawable.home_unselected, "Home")).setTag("home"));
        tabLayout.addTab(tabLayout.newTab().setCustomView(getCustomTabView(R.drawable.meal_unselected, "Orders")).setTag("meal_plan"));
        tabLayout.addTab(tabLayout.newTab().setCustomView(getCustomTabView(R.drawable.coach_unselected, "Reminder")).setTag("coach"));
        tabLayout.addTab(tabLayout.newTab().setCustomView(getCustomTabView(R.drawable.more_unselected, "More")).setTag("more"));
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#F48120"));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tabSelectedView(tab);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tabUnSelectedView(tab);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                if (!tabReselect || getSupportFragmentManager().getBackStackEntryCount() > 1) {
                    tabReselect = true;
                    tabSelectedView(tab);
                }
            }
        });
    }

    private void tabUnSelectedView(TabLayout.Tab tab) {
        Object tag = tab.getTag();
        if (tag != null) {
            String tagStr = tag.toString();
            Log.d("Tap String", tagStr);
            switch (tagStr) {
                case "home":
                    reuseCustomTabView((LinearLayout) tab.getCustomView(), R.drawable.home_unselected, false);
                    break;
                case "meal_plan":
                    reuseCustomTabView((LinearLayout) tab.getCustomView(), R.drawable.meal_unselected, false);
                    break;
                case "coach":
                    reuseCustomTabView((LinearLayout) tab.getCustomView(), R.drawable.coach_unselected, false);
                    break;
                case "more":
                    reuseCustomTabView((LinearLayout) tab.getCustomView(), R.drawable.more_unselected, false);
                    break;
            }
        }
    }

    private void tabSelectedView(TabLayout.Tab tab) {
        Object tag = tab.getTag();
        if (tag != null) {
            switch (tag.toString()) {
                case "home":
//                    setWorkingPhaseAndCurrentDate();
//                    reuseCustomTabView((LinearLayout) tab.getCustomView(), R.drawable.home_selected, true);
//                    Log.d("Tap String", "Home Screen");
//                    if (dietChart != null && dietChart.ID != null) {
//                        session.setDietChartIdForHome(dietChart.ID);
//                    }
                    popBackStack();
                    startNewFragment(new DescriptionFragment(), Constants.DESCRIPTION_FRAGMENT);
                    break;
                case "meal_plan":
//                    if (AppPreference.getInt(Constants.CAN_USE_APP,TWApp.appContext) == 1||AppPreference.getInt(Constants.CAN_USE_APP,TWApp.appContext) == 0 || AppPreference.getInt(Constants.CAN_USE_APP,TWApp.appContext) == -1) {
//                        reuseCustomTabView((LinearLayout) tab.getCustomView(), R.drawable.meal_selected, true);
//                        final Date today = new Date();
//                        this.workingDate = sdf.format(today);
//                        this.session.setWorkingDate(workingDate);
//                        newfrag = new DietPhaseDetailsFragment();//.newInstance(session.getWorkingPhase(), this.workingDate, Utility.getCurrentMealTime(), null);
//                        popBackStack();
//                        startNewFragment(newfrag, "meal_plan");
//                        break;
//                    }else {
//                        alert.showAlertMealNotallowed(HomeActivity.this,"Alert","As per our records, your coach has updated that you will not be able to log food. Meal plan page is available only to people who can use the app and log food. Incase you want to access this feature, please write to your coach to change your settings. Or you can access your meal plans in your emails.",false);
//                        notificationSelected(menuSelection.HOME.toString());
//                        break;
//                    }
                    popBackStack();
                    startNewFragment(new MyOrdersFragment(), Constants.MY_ORDERS_FRAGMENT);
                    break;
                case "coach":
//                    reuseCustomTabView((LinearLayout) tab.getCustomView(), R.drawable.coach_selected, true);
                   // popBackStack();
                   // startNewFragment(new DescriptionFragment(), Constants.MY_ORDERS_FRAGMENT);
                    break;
                case "more":
//                    reuseCustomTabView((LinearLayout) tab.getCustomView(), R.drawable.more_selected, true);
//                    newfrag = new MoreItemFragment();
//                    popBackStack();
//                    startNewFragment(newfrag, "more");
                    break;
            }
        }
    }

    private LinearLayout getCustomTabView(int drawableId, String text) {
        LinearLayout tabIconLayout = (LinearLayout) LayoutInflater.from(this).inflate(R.layout.custom_tab_icon, null);
        TextView tabIconText = (TextView) tabIconLayout.findViewById(R.id.tab_text);
        ImageView tabIconImage = (ImageView) tabIconLayout.findViewById(R.id.tab_icon);
        tabIconText.setText(text);
        tabIconImage.setImageDrawable(getResources().getDrawable(drawableId));
        return tabIconLayout;
    }

    private void reuseCustomTabView(LinearLayout tabLayout, int drawableId, boolean isFromSelected) {
        TextView tabIconText = (TextView) tabLayout.findViewById(R.id.tab_text);
        ImageView tabIconImage = (ImageView) tabLayout.findViewById(R.id.tab_icon);
        if (isFromSelected) {
            tabIconText.setTextColor(Color.parseColor("#F48120"));
        } else {
            tabIconText.setTextColor(Color.parseColor("#A3A3A3"));
        }
        tabIconImage.setImageDrawable(getResources().getDrawable(drawableId));
    }

    public void popBackStack() {
        final FragmentManager fm = this.getSupportFragmentManager();
        for (int i = 0; i < fm.getBackStackEntryCount(); i++) {
            try {
                fm.popBackStack();
            } catch (IllegalStateException ignored) {
                ignored.printStackTrace();
                // There's no way to avoid getting this if saveInstanceState has already been called.
            }
        }
    }

}
