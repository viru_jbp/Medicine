package com.online.medicine.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import com.online.medicine.R;
import com.online.medicine.application.MedicineApp;
import com.online.medicine.fragments.LoginMainFragment;
import com.online.medicine.fragments.SignInFragment;
import com.online.medicine.fragments.SignUpFragment;
import com.online.medicine.listeners.FragmentInteractionListener;
import com.online.medicine.utils.Constants;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity implements FragmentInteractionListener{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        startNewFragment(new LoginMainFragment(), Constants.LOGIN_MAIN_FRAGMENT);
    }

    @Override
    public void onFragmentInteraction(String id, Object data) {
        Fragment fragment;
        switch (id) {
            case Constants.SIGN_UP_FRAGMENT:
                fragment = new SignUpFragment();
                startNewFragment(fragment, Constants.SIGN_UP_FRAGMENT);
                break;
            case Constants.SIGN_IN_FRAGMENT:
                fragment = new SignInFragment();
                startNewFragment(fragment, Constants.SIGN_IN_FRAGMENT);
                break;
            case Constants.SKIP_BUTTON:
                startMainActivity();
                break;
        }
    }

    private void startNewFragment(final Fragment frag, final String tag) {
        final FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        if (this.getSupportFragmentManager().findFragmentById(R.id.container) != null) {
            fragmentTransaction.replace(R.id.container, frag, tag);
            fragmentTransaction.addToBackStack(null);
        } else {
            fragmentTransaction.add(R.id.container, frag, tag);
        }
        //fragmentTransaction.commit();
        fragmentTransaction.commitAllowingStateLoss();
    }

    private void startMainActivity(){
        MedicineApp.skipLogin = true;
        Intent i = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

}

