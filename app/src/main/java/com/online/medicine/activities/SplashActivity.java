package com.online.medicine.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.online.medicine.R;

public class SplashActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 1000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startMainScreenWithDelay();
    }

    private void startMainScreenWithDelay(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                mainIntent();
            }
        }, SPLASH_TIME_OUT);
    }

    private void mainIntent() {
        Intent i = new Intent(SplashActivity.this, MainActivity.class);
        startActivity(i);
        finish();
    }

}
