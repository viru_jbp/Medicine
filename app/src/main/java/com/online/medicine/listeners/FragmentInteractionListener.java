package com.online.medicine.listeners;

public interface FragmentInteractionListener {
    void onFragmentInteraction(String id, Object data);
}
