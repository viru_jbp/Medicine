package com.online.medicine.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.support.v7.widget.AppCompatTextView;

public class SquareTextView extends AppCompatTextView {


    public SquareTextView(final Context context) {
        super(context);
    }

    public SquareTextView(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(final int widthMeasureSpec, final int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        int size = 0;
        final int width = this.getMeasuredWidth();
        final int height = this.getMeasuredHeight();

        if (width < height) {
            size = height;
        } else {
            size = width;
        }
        this.setMeasuredDimension(size, size);
    }
}
