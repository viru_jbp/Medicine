package com.online.medicine.utils;

public interface Constants {
    // App Preference Constants
    String IS_LOGIN = "IsLoggedIn";

    // Fragment Constants
    String LOGIN_MAIN_FRAGMENT = "login_main_fragment";
    String SIGN_UP_FRAGMENT = "sign_up_fragment";
    String SIGN_IN_FRAGMENT = "sign_in_fragment";
    String SKIP_BUTTON = "skip_button";
    String DESCRIPTION_FRAGMENT = "description_fragment";
    String MY_ACCOUNT_FRAGMENT = "my_account_fragment";
    String MY_CART_FRAGMENT = "my_cart_fragment";
    String MY_ORDERS_FRAGMENT = "my_order_fragment";
    String LOGOUT_FRAGMENT = "logout_fragment";
}
